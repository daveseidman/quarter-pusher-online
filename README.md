# Quarter Pusher Online

Livestream a quarter pusher, embed it within a site that lets visitors pay to play and collect winnings.


## Problems to solve:

- Payment / Payout system
- Queuing system
- Streaming system <- Youtube
- Quarter feeder
- Winnings counter <- postage scale
- Emergency shutoff


## Other considerations:

- Turn a profit? Charge 50 cents to play one quarter? Or a small percentage to cover streaming / electric / internet costs? Allow advertisers?


## Research:

http://www.retrobuiltgames.com/the-build-page/coin-pusher-w-arduino/
